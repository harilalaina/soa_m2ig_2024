import Joi from "joi";

const demandeSchema = Joi.object({
    typeConge: Joi.string().required(),
    motif: Joi.string().required(),
    etatConge: Joi.string().required(),
    dureeConge: Joi.string().required(),
    dateDebut: Joi.string().required(),
    dateFin: Joi.string().required(),
});

export const validateDemande = (demande) => {
    return demandeSchema.validate(demande);
}