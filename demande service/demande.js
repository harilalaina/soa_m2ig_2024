import Sequelize from "sequelize";
import dotenv from "dotenv";

dotenv.config();

export const initDemande = () => {
  console.log("DB_NAME: ", process.env.DB_NAME);
  const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      dialect: "postgres",
    }
  );

  const Demande = sequelize.define(
    "demande",
    {
      typeConge: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      motif: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      etatConge: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      dureeConge: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      dateDebut: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      dateFin: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    {
        primaryKey: true,
    }
  );

  sequelize.sync(); // Create the table in the database
  return Demande;
};
