import express from "express";
import cors from "cors";
import { initDemande } from "./demande.js";
import { validateDemande } from "./validation.js";

const app = express();

app.use(cors());
app.use(express.json());

const Demande = initDemande();

// Define the route handlers
const createDemande = async (req, res) => {
  const validationResult = validateDemande(req.body);

  if (validationResult.error) {
    return res
      .status(400)
      .json({ message: validationResult.error.details[0].message });
  }
  const demande = await Demande.create(req.body);
  res.status(201).json(demande);
};

const getDemandes = async (req, res) => {
  const demandes = await Demande.findAll();
  res.json(demandes);
};

const getDemande = async (req, res) => {
  const demande = await Demande.findByPk(req.params.id);
  if (demande) {
    res.json(demande);
  } else {
    res.sendStatus(404);
  }
};

const updateDemande = async (req, res) => {
  const demande = await Demande.findByPk(req.params.id);
  if (demande) {
    const validationResult = validateDemande(req.body);

    if (validationResult.error) {
      return res
        .status(400)
        .json({ message: validationResult.error.details[0].message });
    }
    await demande.update(req.body);
    res.json(demande);
  } else {
    res.sendStatus(404);
  }
};

const deleteDemande = async (req, res) => {
  const demande = await Demande.findByPk(req.params.id);
  if (demande) {
    await demande.destroy();
    res.sendStatus(204);
  } else {
    res.sendStatus(404);
  }
};

app.post("/", createDemande);
app.get("/", getDemandes);
app.get("/:id", getDemande);
app.put("/:id", updateDemande);
app.delete("/:id", deleteDemande);

// Get the port from an environment variable or use a default value
const port = 4000;

// Start the server
app.listen(port, () => {
  console.log(`server launched on port ${port}`);
});
