import express from "express";
import cors from "cors";
import { initEmployee } from "./employee.js";
import { validateEmployee } from "./validation.js";

const app = express();

app.use(cors());
app.use(express.json());

const Employee = initEmployee();

// Define the route handlers
const createEmployee = async (req, res) => {
  const validationResult = validateEmployee(req.body);

  if (validationResult.error) {
    return res
      .status(400)
      .json({ message: validationResult.error.details[0].message });
  }
  const employee = await Employee.create(req.body);
  res.status(201).json(employee);
};

const getEmployees = async (req, res) => {
  const employees = await Employee.findAll();
  res.json(employees);
};

const getEmployee = async (req, res) => {
  const employee = await Employee.findByPk(req.params.id);
  if (employee) {
    res.json(employee);
  } else {
    res.sendStatus(404);
  }
};

const updateEmployee = async (req, res) => {
  const employee = await Employee.findByPk(req.params.id);
  if (employee) {
    const validationResult = validateEmployee(req.body);

    if (validationResult.error) {
      return res
        .status(400)
        .json({ message: validationResult.error.details[0].message });
    }
    await employee.update(req.body);
    res.json(employee);
  } else {
    res.sendStatus(404);
  }
};

const deleteEmployee = async (req, res) => {
  const employee = await Employee.findByPk(req.params.id);
  if (employee) {
    await employee.destroy();
    res.sendStatus(204);
  } else {
    res.sendStatus(404);
  }
};

app.post("/", createEmployee);
app.get("/", getEmployees);
app.get("/:id", getEmployee);
app.put("/:id", updateEmployee);
app.delete("/:id", deleteEmployee);

// Get the port from an environment variable or use a default value
const port = 4000;

// Start the server
app.listen(port, () => {
  console.log(`server launched on port ${port}`);
});
