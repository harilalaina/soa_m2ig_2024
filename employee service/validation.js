import Joi from "joi";

const employeeSchema = Joi.object({
    nom: Joi.string().min(2).max(50).required(),
    email: Joi.string().email().required(),
    poste: Joi.string().required(),
});

export const validateEmployee = (employee) => {
    return employeeSchema.validate(employee);
}