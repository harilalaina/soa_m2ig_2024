import Sequelize from "sequelize";
import dotenv from "dotenv";

dotenv.config();

export const initEmployee = () => {
  console.log("DB_NAME: ", process.env.DB_NAME);
  const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      dialect: "postgres",
    }
  );

  const Employee = sequelize.define(
    "employee",
    {
      nom: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      poste: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    {
        primaryKey: true,
    }
  );

  sequelize.sync(); // Create the table in the database
  return Employee;
};
