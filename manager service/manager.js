import Sequelize from "sequelize";
import dotenv from "dotenv";

dotenv.config();

export const initManager = () => {
  console.log("DB_NAME: ", process.env.DB_NAME);
  const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      dialect: "postgres",
    }
  );

  const Manager = sequelize.define(
    "manager",
    {
      idManager: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      nomManager: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      emailManager: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      niveauAcces: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      departement: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    {
        primaryKey: true,
    }
  );

  sequelize.sync(); // Create the table in the database
  return Manager;
};
