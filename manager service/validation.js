import Joi from "joi";

const managerSchema = Joi.object({
    idManger: Joi.number().integer().required(),
    nomManager: Joi.string().min(4).max(50).required(),
    emailManager: Joi.string().email().required(),
    niveauAcces: Joi.string().required(),
    departement: Joi.string().required(),
});

export const validateManager = (manager) => {
    return managerSchema.validate(manager);
}